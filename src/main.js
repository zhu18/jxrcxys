// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'babel-polyfill'
import Vue from 'vue'
import App from './App'
import router from './router'
import echarts from 'echarts'
import ElementUI from 'element-ui'
import store from './store'
import 'assets/css/index.scss'
import 'element-ui/lib/theme-chalk/index.css'
import xhr from 'api/api.js'
import moment from 'moment'

Vue.prototype.$echarts = echarts;
Vue.prototype.$xhr = xhr;
Vue.prototype.$echarts = echarts;
Vue.prototype.$moment = moment;

Vue.config.productionTip = false
Vue.use(ElementUI);

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
