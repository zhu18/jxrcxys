const isLogOpen = true;

export function consoleLog(title, msg) {
  if (!title) {
    title = '输出日志';
  }
  console.log(title, msg);
}



export function checkReg(format, value) {
  let isPass = true;
  let reg = new RegExp(format);
  isPass = reg.test(value);
  return isPass;
}

//电话号码
export const reg_telnum='^1[3|4|5|8][0-9]{9}$';
//正整数
export const reg_num='^[1-9]\\d*$';
//邮编
export const reg_post='^[1-9][0-9]{5}$';

export const reg_net='^(www.[A-Za-z0-9]{1,15})(.com|.cn)*$';
