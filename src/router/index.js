import Vue from 'vue'
import Router from 'vue-router'
import home from 'pages/home/home'
import login from 'pages/login/login'
import creditctrl from 'pages/creditctrl/creditctrl'
import morelist from 'pages/home/morelist'
import dataaccess from 'pages/dataaccess/dataaccess'
import datadetail from 'pages/home/datadetail'
import sysmanage from 'pages/sysmanage'
import serveraccess from 'pages/sysmanage/servermanage/serveraccess'
import serverpublish from 'pages/sysmanage/servermanage/serverpublish'
import serverlog from 'pages/sysmanage/serverlog/serverlog'
import userlist from 'pages/sysmanage/usermanage/userlist'
import useredit from 'pages/sysmanage/usermanage/useredit'
import managerlog from 'pages/sysmanage/usermanage/managerlog'
import rolelist from 'pages/sysmanage/authmanage/rolelist'
import roleedit from 'pages/sysmanage/authmanage/roleedit'
import adminprivilege from 'pages/sysmanage/authmanage/adminprivilege'

Vue.use(Router)


let router = new Router({
  routes: [
    {
      path: '/', //路由默认跳转
      redirect: 'home'
    },
    {
      path: '/home',
      name: 'home',
      component: home,
    },
    {
      path: '/home/morelist',//首页查看更多
      name: 'morelist',
      component: morelist,
    },
    {
      path: '/dataaccess',
      name: 'dataaccess',
      component: dataaccess,
    },
    {
      path: '/datadetail',
      name: 'datadetail',
      component: datadetail,
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/creditctrl',
      name: 'creditctrl',
      component: creditctrl
    },
    {
      path: '/sysmanage',
      name: 'sysmanage',
      component: sysmanage,
      redirect: '/serveraccess',
      children: [
        {
          path: '/serveraccess',
          name: 'serveraccess',
          parentName: 'sysmanage',
          component: serveraccess,
        },
        {
          path: '/serverpublish',
          name: 'serverpublish',
          parentName: 'sysmanage',
          component: serverpublish
        },
        {
          path: '/serverlog',
          name: 'serverlog',
          parentName: 'sysmanage',
          component: serverlog
        },
        {
          path: '/userlist',
          name: 'userlist',
          parentName: 'sysmanage',
          component: userlist
        },
        {
          path: '/useredit',
          name: 'useredit',
          parentName: 'sysmanage',
          component: useredit
        },
        {
          path: '/managerlog',
          name: 'managerlog',
          parentName: 'sysmanage',
          component: managerlog
        },
        {
          path: '/rolelist',
          name: 'rolelist',
          parentName: 'sysmanage',
          component: rolelist
        },
        {
          path: '/roleedit',
          name: 'roleedit',
          parentName: 'sysmanage',
          component: roleedit
        },
        {
          path: '/adminprivilege',
          name: 'adminprivilege',
          parentName: 'sysmanage',
          component: adminprivilege
        },
      ]
    },

  ],
  linkActiveClass: 'active'
})


export default router
