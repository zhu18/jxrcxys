export const ERR_OK = 200

export const TIMEOUT = 10000

export const STATUS = 'code' // 后台返回的状态码，如 code status 这些

export const baseURL = {
  // dev: 'http://localhost:8080/',
  dev: 'http://211.94.139.14:9080/',
  prod: '/'
}
